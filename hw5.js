const doc = {
    header: 'header',
    body: 'body',
    footer: 'footer',
    date: 'date',
    app : {
        header: 'header',
        body: 'body',
        footer: 'footer',
        date: 'date'
    }
};

function Document(obj) {
    this.header = obj.header;
    this.body = obj.body;
    this.footer = obj.footer;
    this.date = obj.date;
    this.app = {
        header: obj.app.header,
        body: obj.app.body,
        footer: obj.app.footer,
        date: obj.app.date
    };
}
Document.prototype.fillObj = function(data) {
    this.header = data.header;
    this.body = data.body;
};
Document.prototype.showObj = function() {
    console.log(this); 
};

const document = new Document(doc);

// document.showObj();

document.fillObj({header: 'newheader', body: 'newbody'});

document.showObj();
