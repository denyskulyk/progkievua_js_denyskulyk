// 1)
let a;
let b;
let result;

if (a + b < 4) {
    result = 'Мало';
  } else {
    result = 'Много';
  }

result = (a + b < 4) ? 'Мало' : 'Много'; 

// 2)
let login;
let message;

if (login == 'Сотрудник') {
  message = 'Привет';
} else if (login == 'Директор') {
  message = 'Здравствуйте';
} else if (login == '') {
  message = 'Нет логина';
} else {
  message = '';
}

let newMessage = (login == 'Сотрудник') ? 'Привет' :
(login == 'Директор') ? 'Здравствуйте' :
(login == '') ? 'Нет логина' :
'';

// // 3)

let c = 1; 
let d = 10;

let number = c - (a%2)+1;

while(number <= d) {
  console.log(number);
  number += 2;
}

// // 4)

// // Прямоугольник
let horizontal = 10;
let vertical = 5;
let stars = '*'.repeat(12);

console.log(stars);

for (let i = 0; i < vertical - 2; ++i) {
  console.log('()' + (' '.repeat(horizontal - 2)) + '()');
}
console.log(stars);

// Прямоугольный треугольник

for( let i = 1; i <= 10; i++){
  console.log("*".repeat(i));
}

// // Равносторонний треугольник 

pyramid(10); 

function pyramid(n) { 
  for (let i = 0; i < n; i++) { 
    let str = ''; 
    for (let j = 1; j < n-i; j++) { 
      str = str + ' '; 
    } 
    for (let k = 1; k <= (2*i+1); k++) { 
      str = str + '*'; 
    } 
    console.log(str); 
  } 
}

// // Ромб

let backspaces = 18;
let rombStars = 1;
let lines = 20;

for (let i = 0; i <= lines; i++){

    for (let j = 0; j <= backspaces; j++){
        document.write("&nbsp");
    }
    for (let k = 0; k < rombStars; k++){
        document.write("*");
    }

    backspaces--;
    rombStars++;

    if (i >= lines / 2 && i <= lines) {
        rombStars = rombStars - 2;
        backspaces = backspaces + 2;
    }
    document.write("<br>");
}

