// Задание с функцией map.

const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9];

const func = function(item) {
    console.log([item]);
};

const map = function(fn, array) {
    array.forEach(item => {
        fn(item);
    });
};

map(func, arr);


// C помощью ?

function checkAge(age) {
    return age > 18 ? true : confirm('Родители разрешили?');
}

// С помощью ||

function checkAge(age) {
    return age > 18 || confirm('Родители разрешили?');
}